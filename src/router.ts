import Vue from "vue";
import Router from "vue-router";
import HomePage from "./views/Home.vue";
import StatsPage from "./views/Stats.vue";
import StatsPeoplePage from "./views/StatsPeople.vue";
import StatsWeddingPage from "./views/StatsWedding.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage
    },
    {
      path: "/stats",
      name: "stats",
      component: StatsPage
    },
    {
      path: "/stats-people",
      name: "stats-people",
      component: StatsPeoplePage
    },
    {
      path: "/stats-wedding",
      name: "stats-wedding",
      component: StatsWeddingPage
    }
  ]
});
