let axios = require("axios");

const instance = axios.create({
  baseURL: "https://api.hack.midvikus.com/"
});

const target = {};
const handler = {
  get(target: any, name: string) {
    name = name.replace(/__/g, "/");
    return Object.assign(
      {},
      ["get", "delete", "options", "head"].reduce(
        (o, method) =>
          Object.assign({}, o, {
            [method](url = "", params = {}) {
              if (typeof url === "object") {
                params = url;
                url = "";
              }
              if (url[url.length - 1] !== "/") {
                url += "/";
              }
              return instance[method](name + url, { params });
            }
          }),
        {}
      ),
      ["post", "put", "patch"].reduce(
        (o, method) =>
          Object.assign({}, o, {
            [method](url = "", body = {}, params = {}) {
              if (typeof url === "object") {
                params = body;
                body = url;
                url = "";
              }

              if (url[url.length - 1] !== "/") {
                url += "/";
              }
              return instance[method](name + url, body, { params });
            }
          }),
        {}
      )
    );
  }
};

export default new Proxy(target, handler);
